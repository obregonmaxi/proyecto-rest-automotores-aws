package com.automotores.repository;

import org.springframework.data.repository.CrudRepository;

import com.automotores.model.Automotor;

public interface AutomotorRepository extends CrudRepository<Automotor, Integer> {

}
