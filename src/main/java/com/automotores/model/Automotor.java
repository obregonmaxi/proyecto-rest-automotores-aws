package com.automotores.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.automotores.converter.StringToListConverter;

@Entity
@Table(name = "automotor")
public class Automotor {

	@Id
	@Column(name = "ID_AUTOMOTOR")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "NOMBRE")
	private String nombre;

	@Convert(converter = StringToListConverter.class)
	@Column(name = "OPCIONALES")
	private List<String> opcionales;

	@Column(name = "COSTO")
	private Integer costo;

	public Automotor(Automotor automotor) {
		this.nombre = automotor.nombre;
		this.opcionales = automotor.opcionales;
		this.id = automotor.id;
	}

	public Automotor() {
	}

	public void calcularCosto() {
		Integer costo = 0;

		switch (getNombre()) {
		case "Sedan":
			costo = 230000;
			break;
		case "Familiar":
			costo = 245000;
			break;
		case "Coupe":
			costo = 270000;
			break;
		default:
			break;
		}

		for (String string : getOpcionales()) {
			switch (string) {
			case "TC":
				costo = costo + 12000;
				break;
			case "AA":
				costo = costo + 20000;
				break;
			case "ABS":
				costo = costo + 14000;
				break;
			case "AB":
				costo = costo + 7000;
				break;
			case "LL":
				costo = costo + 12000;
				break;
			default:
				break;
			}
		}
		setCosto(costo);
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<String> getOpcionales() {
		return opcionales;
	}

	public void setOpcionales(List<String> opcionales) {
		this.opcionales = opcionales;
	}

	public Integer getCosto() {
		return costo;
	}

	public void setCosto(Integer costo) {
		this.costo = costo;
	}

}
