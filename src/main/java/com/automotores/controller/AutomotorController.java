package com.automotores.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.automotores.model.Automotor;
import com.automotores.repository.AutomotorRepository;

@RestController
@RequestMapping(path = "/automotores")
public class AutomotorController {

	@Autowired
	private AutomotorRepository repository;

	@GetMapping
	public Iterable<Automotor> all() {
		return repository.findAll();
	}

	@GetMapping("/{id}")
	public Automotor get(@PathVariable Integer id) {
		return repository.findById(id).get();
	}

	@PostMapping
	public Automotor create(@RequestBody Automotor automotor) {
		Automotor nuevoAutomotor = new Automotor(automotor);
		nuevoAutomotor.calcularCosto();
		return repository.save(nuevoAutomotor);
	}

	@DeleteMapping("/{id}")
	public void delete(@PathVariable Integer id) {
		repository.deleteById(id);
	}

}
